import axios from 'axios';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import entityManager from '../src/common/entityManager';
import { describe } from "mocha";
import { expect } from 'chai';
import { Account } from "../src/models/account";
import { Transaction } from '../src/models/transaction';
import { Controller } from '../src/common/controller';
import { TransactionController } from '../src/controllers/transactionController';
import { App } from '../src/app';

chai.use(chaiAsPromised);

describe('Transaction tests', () => {

    let app: App;

    before(() => {
        const controllers: Controller[] = [
            new TransactionController()
        ];
        
        app = App.start(controllers, 2112);
    });

    it('Can insert with new account', async () => {
        const account = new Account({
            id: '111',
            name: 'Dimas Rizward',
            email: 'rizwardh@gmail.com',
        });
        const transaction = new Transaction({
            deposit: 400000,
            account
        });

        await expect(axios.post('http://localhost:2112/transactions', transaction))
                .to
                .be
                .not
                .rejected;
    })

    it('Can insert with existing account', async () => {
        const transaction = new Transaction({
            deposit: 200000,
            account: '111' as any
        });

        await expect(axios.post('http://localhost:2112/transactions', transaction))
                .to
                .be
                .not
                .rejected;
    })

    it('Will throw when account isnt exists', async () => {
        const transaction = new Transaction({
            deposit: 200000,
            account: '222' as any
        });

        await expect(axios.post('http://localhost:2112/transactions', transaction))
                .to
                .be
                .rejected;
    })

    after(async () => {
        const em = await entityManager();
        await em.dropDatabase();

        app.server.close();
    })

});