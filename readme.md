# TNIS Test

## Run

1. Simply run ```npm install```
2. Change the database configuration in ```src/common/entityManager.ts```
3. Change email auth config in ```src/common/mailer.ts```
4. Then run ```npm run serve```

## Test

1. Run ```npm run test```
