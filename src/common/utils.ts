
import express from 'express';
import { QueryInfo } from './queryInfo'

const utils = {

    sendError(res: express.Response, message: string, code: number = 400) {
        return res.status(code)
           .json({
               status: 'error',
               message,
           })
    },

    sendSuccess(res: express.Response, data: any, message: string, queryInfo?: {
        limit?: QueryInfo
        page?: QueryInfo,
        length?: QueryInfo
    }) {
        const payload = {
            status: 'success',
            message,
            result: data
        };

        if(queryInfo)
            payload['queryInfo'] = queryInfo;

        return res.status(200)
                  .json(payload)
    }

}

export default utils;