
export interface QueryInfo {
    current: number;
    max: number;
}