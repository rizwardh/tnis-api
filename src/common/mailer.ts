
import nodeMailer from 'nodemailer';

async function sendEmail(to: string, subject: string, text: string) {
    const tr = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'huehue@gmail.com',
            pass: 'huehue'
        }
    });

    const option = {
        from: '"Dimas Rizward" <rizward@gmail.com>',
        to,
        subject,
        text
    };

    return await tr.sendMail(option);
}

export default sendEmail;