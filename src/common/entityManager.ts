import { Account } from "../models/account";
import { Transaction } from "../models/transaction";
import { createConnection } from "typeorm";

const connection = () => {
    return createConnection({
        type: "postgres",
        host: "localhost",
        port: 5432,
        username: "postgres",
        password: "master",
        database: "skrpsi",
        entities: [
            Account, Transaction
        ],
        synchronize: true
    });
};

export default connection;