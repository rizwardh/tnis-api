import 'reflect-metadata';
import { App } from './app';
import { Controller } from './common/controller';
import { TransactionController } from './controllers/transactionController';
import { AccountController } from './controllers/accountController';
 
const controllers: Controller[] = [
    new TransactionController(),
    new AccountController()
];

App.start(controllers, 2112);