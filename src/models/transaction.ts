
import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Account } from './account';

@Entity()
export class Transaction {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    deposit: number;

    @Column({ type: 'timestamp with time zone', default: () => 'CURRENT_TIMESTAMP' })
    date: string;

    @ManyToOne(on => Account)
    account: Account;

    constructor(props?: Partial<Transaction>) {
        Object.assign(this, props);
    }

}