import { PrimaryColumn, Column, Entity } from "typeorm";
import moment from "moment";

@Entity()
export class Account {

    @PrimaryColumn()
    id: string;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column({ type: 'timestamp with time zone', default: () => 'CURRENT_TIMESTAMP' })
    createdDate: string;

    constructor(props?: Partial<Account>) {
        Object.assign(this, props);
    }

}