import express from 'express';
import utils from '../common/utils';
import entityManager from "../common/entityManager";
import { Account } from '../models/account';
import { Controller } from "../common/controller";

export class AccountController implements Controller {
    
    router = express.Router();
    basePath = '/accounts';

    private readonly props: Array<keyof Account> = [
        'name', 'email'
    ];
    
    constructor() {
        this.router.get('/', this.getAll());
        this.router.get('/count', this.getCount());
        this.router.get('/:id', this.get());
        this.router.post('/', this.post());
    }

    getAll() {
        return async (req: express.Request, res: express.Response) => {
            const page = +req.query.page || 1;
            let limit = +req.query.limit || 100;
            if(limit > 1000)
                limit = 1000;

            const em = await entityManager();
            const repo = em.getRepository(Account);
                
            const maxLength = await repo.count();
            const data = await repo.find({ take: limit, skip: (page - 1) * limit });

            em.close();
            return utils.sendSuccess(res, data, 'Data found!', {
                limit: { current: limit, max: 1000 },
                length: { current: data.length, max: maxLength },
                page: { current: page, max: Math.round(maxLength / page) }
            });
        }
    }

    get() {
        return async (req: express.Request, res: express.Response) => {
            const em = await entityManager();
            const repo = em.getRepository(Account);

            const id = req.params.id;
            const account = await repo.findOne(id);
            
            em.close();
            if(!account)
                return utils.sendError(res, `Account with id ${req} found`);

            return utils.sendSuccess(res, account, `Account found!`);
        };
    }

    post() {
        return async (req: express.Request, res: express.Response) => {
            const model = req.body;
            
            if(!this.props.every(x => model.hasOwnProperty(x)))
                return utils.sendError(res, `Missing one property of ${this.props.join(', ')}`);
                
            const em = await entityManager();
            const account = new Account(model);

            try {
                await em.manager.save(account);
                return utils.sendSuccess(res, null, `New account with id ${model.id} created`);
            }
            catch(err) {
                return utils.sendError(res, `Unexpected error! Details: ${err.message || err}`);
            }
            finally {
                em.close();
            }
            
        } 
    }

    getCount() {
        return async (req: express.Request, res: express.Response) => {
            const em = await entityManager();
            const repo = em.getRepository(Account);

            const count = await repo.count();
            
            em.close();
            return utils.sendSuccess(res, { count }, `Accounts count returned!`);
        };
    }

}