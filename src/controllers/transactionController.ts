import express from 'express';
import entityManager from '../common/entityManager';
import utils from '../common/utils';
import mailer from '../common/mailer';

import { Controller } from "../common/controller";
import { Transaction } from '../models/transaction';
import { FindManyOptions, Between } from 'typeorm';
import { Account } from '../models/account';

export class TransactionController implements Controller {

    router = express.Router();    
    basePath = '/transactions';
    
    constructor() {
        this.router.get('/count', this.getCount());
        this.router.get('/summary/:id', this.getSummary());
        this.router.get('/:id', this.get());
        this.router.post('/', this.post());
        
    }

    get() {
        return async (req: express.Request, res: express.Response) => {
            const page = +req.query.page || 1;
            let limit = +req.query.limit || 100;
            if(limit > 1000)
                limit = 1000;
                
            const id = req.params.id;
            const query: FindManyOptions<Transaction> = { 
                where: { account: { id } }
            }

            const em = await entityManager();
            const repo = em.getRepository(Transaction);

            const maxLength = await repo.count(query);
            const data = await repo.find(Object.assign({
                take: limit, 
                skip: (page - 1) * limit
            }, query)); // Duplicate previous query

            em.close();
            return utils.sendSuccess(res, data, 'Data found!', {
                limit: { current: limit, max: 1000 },
                length: { current: data.length, max: maxLength },
                page: { current: page, max: Math.round(maxLength / page) }
            });
        }
    }

    getSummary() {
        return async (req: express.Request, res: express.Response) => {
            const id = req.params.id;

            const em = await entityManager();
            const repo = em.getRepository(Transaction);

            const {sum} = await repo.createQueryBuilder('tr')
                                  .select('SUM(tr.deposit)', 'sum')
                                  .where('tr.id = :id')
                                  .setParameters({ id })
                                  .getRawOne();

            em.close();
            
            if(!sum)
                return utils.sendError(res, `Cannot get transaction summary of id ${id}`);

            return utils.sendSuccess(res, { summary: sum }, `Get summary of id ${id} success!`);
        };
    }

    post() {
        return async (req: express.Request, res: express.Response) => {
            const model = req.body;

            const mandatory: Array<keyof Transaction> = [ 'deposit', 'account' ];
            if(mandatory.some(x => model[x] == undefined))
                return utils.sendError(res, 'Balance must be present in the body payload');
    
            const em = await entityManager();
            const isExists = typeof model.account == 'string';
            let account: Account;

            const runner = em.createQueryRunner();
            await runner.connect();
            try {
                await runner.startTransaction();

                const transaction = new Transaction(model);
                if(isExists) {
                    account = await runner.manager
                                          .getRepository(Account)
                                          .findOne(model.account);

                    if(!account)
                        throw `Account with id ${model.account} didn't exists!`;
                }
                else {
                    account = new Account(model.account);
                    await runner.manager.save(account);
                }

                delete model.account;
                transaction.account = account;
                
                await runner.manager.save(transaction);
                await runner.commitTransaction();

                mailer(account.email, 'Deposit sukses gan', `Deposit anda sebesar ${transaction.deposit} berhasil gan!`);
                return utils.sendSuccess(res, transaction, `Deposit Rp ${transaction.deposit} for account ${account.id} saved!`);
            }
            catch(err) {
                await runner.rollbackTransaction();
                return utils.sendError(res, `Unexpected error! Details: ${err.message || err}`);
            }
            finally {
                runner.release();
                em.close();
            }
        }
    }

    getCount() {
        return async (req: express.Request, res: express.Response) => {
            const em = await entityManager();
            const repo = em.getRepository(Transaction);

            const count = await repo.count();
            
            em.close();
            return utils.sendSuccess(res, { count }, `Transactions count returned!`);
        };
    }

}