import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { Controller } from './common/controller';
import { Server } from 'http';

export class App {
    public app: express.Application;
    public port: number;
    public server: Server;
    
    private constructor(controllers: Controller[], port: number) {
        const app = this.app = express();
        this.port = port;
        
        app.use(bodyParser.json())
           .use(cors({credentials: true, origin: true}));

        controllers.forEach(x => app.use(x.basePath, x.router));
        this.server = app.listen(port, () => console.log(`Server running on ${port}`));
    }

    static start(controllers: Controller[], port: number) {
        return new App(controllers, port);
    }
}